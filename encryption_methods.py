
from tkinter import filedialog
from Crypto.PublicKey import RSA


def generate_rsa_key_pair(parent, key_length = 1024, dirname =None):

    ##if dirname not given ask for dirname
    if dirname==None:
        dirname = filedialog.askdirectory(parent=parent,initialdir='/',title='Please select a directory')
    
    ## first generate private key

    if dirname[-1] != '\\':
        file_name = dirname + '\\' + 'rsa-private-key.x'
    else:
        file_name = dirname + 'rsa-private-key.x'
    
    file = open(file_name, 'w')

    file.write('---BEGIN OS2 CRYPTO DATA---\n')
    file.write('Description:\n')
    file.write('\tPrivate RSA key\n\n')

    file.write('Method:\n')
    file.write('\tRSA\n\n')

    key = RSA.generate(key_length)
    private_key = key.export_key()

    file.write('Private key:\n')
    write_format_max_char_length(file,private_key.hex())

    file.write('---END OS2 CRYPTO DATA---')
    file.close()

    ##do the same for public key

    if dirname[-1] != '\\':
        file_name = dirname + '\\' + 'rsa-public-key.x'
    else:
        file_name = dirname + 'rsa-public-key.x'

    file = open(file_name, 'w')

    file.write('---BEGIN OS2 CRYPTO DATA---\n')
    file.write('Description:\n')
    file.write('\tPublic RSA key\n\n')

    file.write('Method:\n')
    file.write('\tRSA\n\n')


    public_key = key.public_key().export_key()

    file.write('Public key:\n')

    write_format_max_char_length(file,public_key.hex())
    file.write('---END OS2 CRYPTO DATA---')
    file.close()
    

def write_format_max_char_length(file, data, n=60):
    l = [data[i:i+n] for i in range(0, len(data), n)]
    for line in l:
        file.write('\t{}\n'.format(line))
    file.write('\n')