import tkinter as tk

from encryption_methods import generate_rsa_key_pair

SMALL_FONT = ('Verdana', 5)
MEDIUM_FONT = ('Verdana', 10)
MEDIUM_LARGE_FONT = ('Verdana', 13)
LARGE_FONT  = ('Verdana', 20)
LARGE_LARGE_FONT  = ('Verdana', 30)

#from simple_sym_encrypt import SimpleSymEncrypt
#from simple_sym_decrypt import SimpleSymDecrypt

class MainPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.label_frame_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_title.pack(fill='x', padx = 5, pady=(5,0))

        self.title_label = tk.Label(self.label_frame_title,text = 'Python-Crypto', font=LARGE_LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()


        self.label_frame_sub_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_sub_title.pack(fill='x', padx = 5, pady=(0,40))

        self.sub_title_label = tk.Label(self.label_frame_sub_title,text = 'Napredni Operacijski Sustavi LAB 2', font=MEDIUM_FONT,padx=10, pady=10)
        self.sub_title_label.pack()


        ## btns for pages

        ##Symmetric algorithms

        self.label_frame_1 =tk.LabelFrame(self,width=250, height=125,relief = tk.SOLID,
        borderwidth=0,highlightthickness=0,text = 'Symmetric algorithms', font=MEDIUM_LARGE_FONT,labelanchor = 'n')
        self.label_frame_1.pack(fill='x', padx = 5, pady=5)

        self.simple_sym_encrypt_btn = tk.Button(self.label_frame_1, text = 'ENCRYPTION' , 
        font = MEDIUM_FONT, command = lambda : self.open_choice_raise('SimpleSymEncrypt'))
        self.simple_sym_encrypt_btn.pack(fill='x', padx = 40, pady=5)

        self.simple_sym_decrypt_btn = tk.Button(self.label_frame_1, text = 'DECRYPTION' , 
        font = MEDIUM_FONT, command = lambda : self.open_choice_raise('SimpleSymDecrypt'))
        self.simple_sym_decrypt_btn.pack(fill='x', padx = 40, pady=(5,40))


        ##RSA (Asymmetric algorithm)

        self.label_frame_3 =tk.LabelFrame(self,width=250, height=125,relief = tk.SOLID,
        borderwidth=0,highlightthickness=0,text = 'RSA algorithm', font=MEDIUM_LARGE_FONT,labelanchor = 'n')
        self.label_frame_3.pack(fill='x', padx = 5, pady=5)

        self.rsa_gen_keys_btn = tk.Button(self.label_frame_3, text = 'GENERATE RSA KEY PAIR' , 
        font = MEDIUM_FONT, command = lambda : self.open_choice_raise('RSAKeyGenerator'))
        self.rsa_gen_keys_btn.pack(fill='x', padx = 40, pady=5)

        self.rsa_encrypt_btn = tk.Button(self.label_frame_3, text = 'ENCRYPTION' , 
        font = MEDIUM_FONT, command = lambda : self.open_choice_raise('RSAEncrypt'))
        self.rsa_encrypt_btn.pack(fill='x', padx = 40, pady=5)

        self.rsa_decrypt_btn = tk.Button(self.label_frame_3, text = 'DECRYPTION' , 
        font = MEDIUM_FONT, command = lambda : self.open_choice_raise('RSADecrypt'))
        self.rsa_decrypt_btn.pack(fill='x', padx = 40, pady=5)



    def open_choice(self, page,geometry = '600x600'):
        self.new_window = tk.Toplevel()
        self.new_window.geometry(geometry)
        new_page = page(self.new_window, self)
        new_page.pack(fill='both')
        self.new_window.mainloop()

    def open_choice_raise(self, page):
        self.controller.show_frame(page)


