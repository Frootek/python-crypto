import tkinter as tk
from tkinter import filedialog

import threading
import time

from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP

from progress_bar import AnalyzeProgressBar

from parse_file import parse_rsa_file

import base64


SMALL_FONT = ('Verdana', 5)
MEDIUM_FONT = ('Verdana', 10)
LARGE_FONT  = ('Verdana', 20)
LARGE_LARGE_FONT  = ('Verdana', 30)


class RSADecrypt(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.label_frame_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_title.pack(fill='x', padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_title,text = 'RSA DECRYPTION', font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()


        ## form for symmetric decryption

        #!----------------------------------------------->

        ## import crypted file

        self.label_frame_1 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill='x', padx = 15, pady=5)

        self.crypted_file_label = tk.Label(self.label_frame_1, text = 'Crypted file location', font = MEDIUM_FONT, width = 20, anchor = 'w')
        self.crypted_file_label.pack(side = tk.LEFT)

        self.crypted_file_location = tk.StringVar()
        self.crypted_file_location.trace('w', self.verify)

        self.crypted_file_location_readonly = tk.Entry(self.label_frame_1, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.crypted_file_location_readonly.config(textvariable = self.crypted_file_location, relief='groove')
        self.crypted_file_location_readonly.pack(side = tk.LEFT, padx = 5)

        self.import_crypted_file_loc_btn = tk.Button(self.label_frame_1 , text = 'Import', command = lambda : self.import_x_file(self.crypted_file_location))
        self.import_crypted_file_loc_btn.pack(side = tk.LEFT, padx = 10)

        ## import private key

        self.label_frame_2 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill='x', padx = 15, pady=5)

        self.private_key_label = tk.Label(self.label_frame_2, text = 'Private key location', font = MEDIUM_FONT, width = 20, anchor = 'w')
        self.private_key_label.pack(side = tk.LEFT)

        self.private_key_location = tk.StringVar()
        self.private_key_location.trace('w', self.verify)

        self.private_key_location_readonly = tk.Entry(self.label_frame_2, state = 'readonly',
        readonlybackground='SystemButtonFace', fg='black')

        self.private_key_location_readonly.config(textvariable = self.private_key_location, relief='groove')
        self.private_key_location_readonly.pack(side = tk.LEFT, padx = 5)

        self.import_private_key_btn = tk.Button(self.label_frame_2 , text = 'Import', 
        command = lambda : self.import_x_file(self.private_key_location))
        self.import_private_key_btn.pack(side = tk.LEFT, padx = 10)

        ##

        self.label_frame_3 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_3.pack(fill='x', padx = 15, pady=5)

        self.decrypt_btn = tk.Button(self.label_frame_3,state='disabled',text = 'Decrypt', command = lambda : self.decrypt())
        self.decrypt_btn.pack(fill='x', padx = 5, pady=5)

        self.label_frame_3_5 =tk.LabelFrame(self,width=250, height=50,borderwidth=0,highlightthickness=0)
        self.label_frame_3_5.pack_propagate(0)
        self.label_frame_3_5.pack(fill='x', padx = 15, pady=5)

        self.progress_bar = AnalyzeProgressBar(self.label_frame_3_5,self)

        ##MESSAGE_BOX

        self.label_frame_4 = tk.LabelFrame(self,width=100, height=125,borderwidth=1,highlightthickness=0,
         text = 'Message', font=MEDIUM_FONT,labelanchor = 'n',relief = tk.FLAT)
        self.label_frame_4.pack(fill='x', padx = 5, pady=5)
        

        self.message_entry = tk.Text(self.label_frame_4,height = 10,fg='black',highlightthickness=0,borderwidth=0,state = 'disabled')
        self.message_entry.pack(fill='both',expand = 1, padx=10, pady=10)
    
        ###


        self.label_frame_6 = tk.LabelFrame(self,width=100, height=130,borderwidth=0,highlightthickness=0)
        self.label_frame_6.pack_propagate(0)
        self.label_frame_6.pack(fill='x', padx = 5, pady=5)

        self.label_frame_6_5 = tk.LabelFrame(self,width=100, height=25,borderwidth=0,highlightthickness=0)
        self.label_frame_6_5.pack(fill='x', padx = 5, pady=5)

        self.label_frame_7 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_7.pack(fill='x', padx = 15, pady=5)

        self.back_btn = tk.Button(self.label_frame_7,text = 'Main page', command = lambda : self.controller.show_frame('MainPage'))
        self.back_btn.pack()



    def verify(self,*args):
        key = self.private_key_location.get()
        file = self.crypted_file_location.get()
        if (file[-2:] == '.x' or file[-4:] == '.txt') and (key[-2:] == '.x' or key[-4:] == '.txt'):
            self.decrypt_btn.config(state='normal')
        else:
            self.decrypt_btn.config(state='disabled')


    def import_x_file(self,string_var):
        filename = filedialog.askopenfile(initialdir = '/', title = 'Select file')
        string_var.set(filename.name)

    def decrypt(self):
        ## initialize crypted file first

        #progress_thread = threading.Thread(target = self.progress_bar.start)
        #progress_thread.start()


        self.progress_bar.start_working()
        self.after(1500, self.write_decrypted)


    def write_decrypted(self):
        crypted_file_dict = parse_rsa_file(self.crypted_file_location.get())
        private_key_dict = parse_rsa_file(self.private_key_location.get())

        """
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(crypted_file_dict)

        print("-"*40)
        pp.pprint(private_key_dict)
        """
        data = crypted_file_dict.get('Data')

        if data == None:
            print('No "data" atribute in file: {}'.format(self.crypted_file_location.get()))
            return
        
        key = private_key_dict.get('Private key')

        if key == None:
            print('No "Private key" atribute in file: {}'.format(self.private_key_location.get()))
            return
    
        encoded_key = bytes.fromhex(key).decode('ascii').rstrip('\n')

        data = base64.b64decode(data)

        private_key = RSA.import_key(encoded_key)

        enc_session_key = data[0:private_key.size_in_bytes()]
        x = private_key.size_in_bytes()
        nonce=data[x:x+16]
        x= x + 16
        tag = data[x:x+16]
        x = x + 16
        ciphertext = data[x:]

        # Decrypt the session key with the private RSA key
        cipher_rsa = PKCS1_OAEP.new(private_key)
        session_key = cipher_rsa.decrypt(enc_session_key)

        # Decrypt the data with the AES session key
        cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
        data = cipher_aes.decrypt_and_verify(ciphertext, tag)

        data = data.decode("ascii")

        self.message_entry.config(state = tk.NORMAL)

        self.message_entry.delete("1.0", tk.END)
        self.message_entry.insert("1.0", data)
        self.message_entry.config(state = tk.DISABLED)