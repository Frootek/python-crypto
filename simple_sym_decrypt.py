import tkinter as tk
from tkinter import filedialog

import threading
import time

from Crypto.Cipher import DES3, DES
from Crypto.Cipher import AES

from progress_bar import AnalyzeProgressBar

from parse_file import parse_sym_crypted_file

import base64


SMALL_FONT = ('Verdana', 5)
MEDIUM_FONT = ('Verdana', 10)
LARGE_FONT  = ('Verdana', 20)
LARGE_LARGE_FONT  = ('Verdana', 30)


ALGORITHMS_DICT = {
    'AES' : AES,
    'DES' : DES,
    'DES3': DES3

}
MODES_DICT = {
    'AES' : {
        'ECB' : AES.MODE_ECB,
        'CBC' : AES.MODE_CBC,
        'CFB' : AES.MODE_CFB,
        'OFB' : AES.MODE_OFB
    },
    'DES3' : {
        'ECB' : DES3.MODE_ECB,
        'CBC' : DES3.MODE_CBC,
        'CFB' : DES3.MODE_CFB,
        'OFB' : DES3.MODE_OFB
    },
    'DES' : {
        'ECB' : DES.MODE_ECB,
        'CBC' : DES.MODE_CBC,
        'CFB' : DES.MODE_CFB,
        'OFB' : DES.MODE_OFB
    }
}

class SimpleSymDecrypt(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.label_frame_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_title.pack(fill='x', padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_title,text = 'MESSAGE DECRYPTION', font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()


        ## form for symmetric decryption

        #!----------------------------------------------->

        ## import crypted file

        self.label_frame_1 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill='x', padx = 15, pady=5)

        self.crypted_file_label = tk.Label(self.label_frame_1, text = 'Crypted file location', font = MEDIUM_FONT, width = 20, anchor = 'w')
        self.crypted_file_label.pack(side = tk.LEFT)

        self.crypted_file_location = tk.StringVar()
        self.crypted_file_location.trace('w', self.verify)

        self.crypted_file_location_readonly = tk.Entry(self.label_frame_1, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.crypted_file_location_readonly.config(textvariable = self.crypted_file_location, relief='groove')
        self.crypted_file_location_readonly.pack(side = tk.LEFT, padx = 5)

        self.import_crypted_file_loc_btn = tk.Button(self.label_frame_1 , text = 'Import', command = lambda : self.import_x_file(self.crypted_file_location))
        self.import_crypted_file_loc_btn.pack(side = tk.LEFT, padx = 10)

        ## import secret key

        self.label_frame_2 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill='x', padx = 15, pady=5)

        self.secret_key_label = tk.Label(self.label_frame_2, text = 'Secret key location', font = MEDIUM_FONT, width = 20, anchor = 'w')
        self.secret_key_label.pack(side = tk.LEFT)

        self.secret_key_location = tk.StringVar()
        self.secret_key_location.trace('w', self.verify)

        self.secret_key_location_readonly = tk.Entry(self.label_frame_2, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.secret_key_location_readonly.config(textvariable = self.secret_key_location, relief='groove')
        self.secret_key_location_readonly.pack(side = tk.LEFT, padx = 5)

        self.import_secret_key_btn = tk.Button(self.label_frame_2 , text = 'Import', command = lambda : self.import_x_file(self.secret_key_location))
        self.import_secret_key_btn.pack(side = tk.LEFT, padx = 10)

        ##

        self.label_frame_3 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_3.pack(fill='x', padx = 15, pady=5)

        self.decrypt_btn = tk.Button(self.label_frame_3,state='disabled',text = 'Decrypt', command = lambda : self.decrypt())
        self.decrypt_btn.pack(fill='x', padx = 5, pady=5)

        self.label_frame_3_5 =tk.LabelFrame(self,width=250, height=50,borderwidth=0,highlightthickness=0)
        self.label_frame_3_5.pack_propagate(0)
        self.label_frame_3_5.pack(fill='x', padx = 15, pady=5)

        self.progress_bar = AnalyzeProgressBar(self.label_frame_3_5,self)

        ##MESSAGE_BOX

        self.label_frame_4 = tk.LabelFrame(self,width=100, height=125,borderwidth=1,highlightthickness=0,
         text = 'Message', font=MEDIUM_FONT,labelanchor = 'n',relief = tk.FLAT)
        self.label_frame_4.pack(fill='x', padx = 5, pady=5)
        

        self.message_entry = tk.Text(self.label_frame_4,height = 10,fg='black',highlightthickness=0,borderwidth=0,state = 'disabled')
        self.message_entry.pack(fill='both',expand = 1, padx=10, pady=10)
    
        ###


        self.label_frame_6 = tk.LabelFrame(self,width=100, height=130,borderwidth=0,highlightthickness=0)
        self.label_frame_6.pack_propagate(0)
        self.label_frame_6.pack(fill='x', padx = 5, pady=5)

        self.label_frame_6_5 = tk.LabelFrame(self,width=100, height=25,borderwidth=0,highlightthickness=0)
        self.label_frame_6_5.pack(fill='x', padx = 5, pady=5)

        self.label_frame_7 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_7.pack(fill='x', padx = 15, pady=5)

        self.back_btn = tk.Button(self.label_frame_7,text = 'Main page', command = lambda : self.controller.show_frame('MainPage'))
        self.back_btn.pack()


    def verify(self,*args):
        key = self.secret_key_location.get()
        file = self.crypted_file_location.get()
        if (file[-2:] == '.x' or file[-4:] == '.txt') and (key[-2:] == '.x' or key[-4:] == '.txt'):
            self.decrypt_btn.config(state='normal')
        else:
            self.decrypt_btn.config(state='disabled')


    def import_x_file(self,string_var):
        filename = filedialog.askopenfile(initialdir = '/', title = 'Select file')
        string_var.set(filename.name)

    def decrypt(self):
        #print(self.secret_key_location.get())
        #print(self.crypted_file_location.get())

        ## initialize crypted file first

        #progress_thread = threading.Thread(target = self.progress_bar.start)
        #progress_thread.start()


        self.progress_bar.start_working()
        self.after(1500, self.write_decrypted)


    def write_decrypted(self):
        crypted_file_dict = parse_sym_crypted_file(self.crypted_file_location.get())
        secret_key_dict = parse_sym_crypted_file(self.secret_key_location.get())

        key_in_hex = secret_key_dict.get('Secret key')
        iv_hex = crypted_file_dict.get('Initialization vector')
        msg_b64 = crypted_file_dict.get('Data')
        algorithm = crypted_file_dict.get('Method')
        mode = crypted_file_dict.get('Mode')


        key_decrypt = bytes.fromhex(key_in_hex)
        msg_b64_to_bytes = base64.b64decode(msg_b64)
        if(iv_hex != None):
            iv_decrypt = bytes.fromhex(iv_hex)
        else:
            iv_decrypt = None


        mode = MODES_DICT[algorithm][mode]
        algorithm = ALGORITHMS_DICT[algorithm]

        if iv_decrypt != None:
            cipher_decrypt = algorithm.new(key_decrypt, mode, iv_decrypt)
        else:
            cipher_decrypt = algorithm.new(key_decrypt, mode)
        
        text_recieved_bytes = cipher_decrypt.decrypt(msg_b64_to_bytes)

        text_recieved = text_recieved_bytes.decode('ascii')

        self.message_entry.config(state = tk.NORMAL)

        self.message_entry.delete("1.0", tk.END)
        self.message_entry.insert("1.0", text_recieved)
        self.message_entry.config(state = tk.DISABLED)

        
        #print(text_recieved)
