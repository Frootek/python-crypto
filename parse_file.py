import pprint


def parse_sym_crypted_file(filelocation):
    #pp = pprint.PrettyPrinter(indent=4)

    file = open(filelocation)
    lines = file.readlines()

    parsed = {}

    current_key = ''
    current_val = ''
    
    for line in lines:
        line = line.strip("\t\n")
        if len(line)>0 and line[1]!='-':
            if line[-1:] == ':':
                if current_key != '':
                    parsed[current_key] = current_val
                current_key = line[:-1]
                current_val = ''
                continue
                
            current_val += line
    
    parsed[current_key] = current_val

    #pp.pprint(parsed)
    return parsed

def parse_rsa_file(filelocation):
    file = open(filelocation)
    lines = file.readlines()

    parsed = {}

    current_key = ''
    current_val = ''
    
    for line in lines:
        line = line.strip("\t\n")
        if len(line)>0 and line[1]!='-':
            if line[-1:] == ':':
                if current_key != '':
                    parsed[current_key] = current_val
                current_key = line[:-1]
                current_val = ''
                continue
                
            current_val += line
    
    parsed[current_key] = current_val

    #pp.pprint(parsed)
    return parsed

if __name__ == "__main__":
    file = 'crypted.txt'
    parse_sym_crypted_file(file)