import tkinter as tk
from tkinter import ttk

import time
import threading


##class for simple progress bar
class AnalyzeProgressBar(ttk.Progressbar):
    def __init__(self, parent,controler):
        super().__init__(parent, orient = tk.HORIZONTAL,mode = 'determinate')

    def start_working(self):
        progress_thread = threading.Thread(target = self.start)
        progress_thread.start()


    def start(self):
        self.pack(fill= 'x', pady='5')
        counter = 0
        self['value']= counter
        while counter<=100:
            self['value'] = counter
            time.sleep(0.0005)
            counter += 1
        self['value'] = 0
        self.pack_forget()