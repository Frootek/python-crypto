
import tkinter as tk

from main_page import MainPage
from simple_sym_encrypt import SimpleSymEncrypt
from simple_sym_decrypt import SimpleSymDecrypt
from rsa_key_generator import RSAKeyGenerator
from rsa_encrypt import RSAEncrypt
from rsa_decrypt import RSADecrypt

PAGES = [MainPage,SimpleSymEncrypt,SimpleSymDecrypt,RSAEncrypt,RSADecrypt,RSAKeyGenerator]

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        super().title('Python-Crypto')
        
        self.geometry('415x670')

        self.frames = {}

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        #add Pages to dictionary
        for Page in PAGES:
            page_name = Page.__name__
            frame = Page(parent=container,controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky='nsew')
        
        #open Main Page
        self.show_frame('MainPage')


    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


app = App()
app.resizable(False, False)
app.mainloop()