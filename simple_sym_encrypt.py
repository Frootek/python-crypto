import tkinter as tk
from tkinter import filedialog

from tkinter import ttk

from progress_bar import AnalyzeProgressBar

from Crypto.Cipher import DES3, DES
from Crypto.Cipher import AES
from Crypto import Random


import base64


SMALL_FONT = ('Verdana', 5)
MEDIUM_FONT = ('Verdana', 10)
LARGE_FONT  = ('Verdana', 20)
LARGE_LARGE_FONT  = ('Verdana', 30)


ALGORITHMS = ['DES3', 'AES']
KEY_DES3 = [56, 112, 168]
KEY_AES = [128, 192, 256]

MODES  = ['ECB' , 'CBC','CFB','OFB']

ALGORITHMS_DICT = {
    'AES' : AES,
    'DES' : DES,
    'DES3': DES3

}
MODES_DICT = {
    'AES' : {
        'ECB' : AES.MODE_ECB,
        'CBC' : AES.MODE_CBC,
        'CFB' : AES.MODE_CFB,
        'OFB' : AES.MODE_OFB
    },
    'DES3' : {
        'ECB' : DES3.MODE_ECB,
        'CBC' : DES3.MODE_CBC,
        'CFB' : DES3.MODE_CFB,
        'OFB' : DES3.MODE_OFB
    },
    'DES' : {
        'ECB' : DES.MODE_ECB,
        'CBC' : DES.MODE_CBC,
        'CFB' : DES.MODE_CFB,
        'OFB' : DES.MODE_OFB
    }
}

class SimpleSymEncrypt(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.label_frame_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_title.pack(fill='x', padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_title,text = 'MESSAGE ENCRYPTION', font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()


        ## form for symmetric encryption


        #algorithm_option
        self.label_frame_1 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill='x', padx = 45, pady=5)

        self.algorithm_label = tk.Label(self.label_frame_1, text = 'Algorithm', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.algorithm_label.pack(side = tk.LEFT)

        self.algorithm_choice = tk.StringVar(value = ALGORITHMS[0])

        self.algorithm_option_menu = tk.OptionMenu(self.label_frame_1, self.algorithm_choice, *ALGORITHMS, command = lambda x=None: self.update_key_options())
        self.algorithm_option_menu.configure(width = 15)
        self.algorithm_option_menu.pack(side = tk.LEFT)
        #!----------------------------------------------->

        #key length 
        self.label_frame_1_5 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1_5.pack(fill='x', padx = 45, pady=5)

        self.key_length_label = tk.Label(self.label_frame_1_5, text = 'Key length', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.key_length_label.pack(side = tk.LEFT)

        self.key_length_choice = tk.StringVar(value = KEY_DES3[0])

        self.key_length_choice_option_menu = tk.OptionMenu(self.label_frame_1_5, self.key_length_choice, *KEY_DES3)
        self.key_length_choice_option_menu.configure(width = 15)
        self.key_length_choice_option_menu.pack(side = tk.LEFT)
        #!----------------------------------------------->

        #algorithm_mode
        self.label_frame_2 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill='x', padx = 45, pady=5)

        self.algorithm_mode_label = tk.Label(self.label_frame_2, text = 'Mode', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.algorithm_mode_label.pack(side = tk.LEFT)

        self.algorithm_mode = tk.StringVar(value = MODES[0])

        self.algorithm_mode_option_menu = tk.OptionMenu(self.label_frame_2, self.algorithm_mode, *MODES)
        self.algorithm_mode_option_menu.configure(width = 15)
        self.algorithm_mode_option_menu.pack(side = tk.LEFT)
        #!----------------------------------------------->

        #file_save_location
        self.label_frame_3 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_3.pack(fill='x', padx = 45, pady=5)

        self.file_save_loc_label = tk.Label(self.label_frame_3, text = 'Save location', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.file_save_loc_label.pack(side = tk.LEFT)

        self.save_location = tk.StringVar(value = 'C:\\')

        self.location_readonly = tk.Entry(self.label_frame_3, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.location_readonly.config(textvariable = self.save_location, relief='groove')
        self.location_readonly.pack(side = tk.LEFT, padx = 2)

        self.change_loc_btn = tk.Button(self.label_frame_3 , text = 'Change', command = lambda : self.update_location())
        self.change_loc_btn.pack(side = tk.LEFT, padx = 10)

        #!----------------------------------------------->

        ##MESSAGE_BOX

        self.label_frame_4 = tk.LabelFrame(self,width=100, height=125,borderwidth=1,highlightthickness=0,
         text = 'Message', font=MEDIUM_FONT,labelanchor = 'n',relief = tk.FLAT)
        self.label_frame_4.pack(fill='x', padx = 5, pady=5)
        

        self.message_entry = tk.Text(self.label_frame_4,height = 10,fg='black',highlightthickness=0,borderwidth=0)
        self.message_entry.pack(fill='both',expand = 1, padx=10, pady=10)

        self.message_entry.bind('<<Modified>>', self.validate_form)



        ##Encrypt btn and also 

        self.label_frame_5 = tk.LabelFrame(self,width=100, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_5.pack(fill='x', padx = 5, pady=5)


        self.encrypt_btn = tk.Button(self.label_frame_5,state='disabled',text = 'Encrypt', command = lambda : self.encrypt())
        self.encrypt_btn.pack(fill='x', padx = 5, pady=5)


        #some space for final button frame
        self.label_frame_6 = tk.LabelFrame(self,width=100, height=130,borderwidth=0,highlightthickness=0)
        self.label_frame_6.pack_propagate(0)
        self.label_frame_6.pack(fill='x', padx = 5, pady=5)

        self.progress_bar = AnalyzeProgressBar(self.label_frame_6,self)

        self.label_frame_7 = tk.LabelFrame(self,width=100, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_7.pack(fill='x', padx = 5, pady=5)

        self.back_btn = tk.Button(self.label_frame_7,text = 'Main page', command = lambda : self.controller.show_frame('MainPage'))
        self.back_btn.pack()



    def validate_form(self, value=None):
        text = self.message_entry.get('1.0','end-1c')
        if(len(text) > 0):
            self.encrypt_btn.config(state='normal')
        else:
            self.encrypt_btn.config(state='disabled')
        self.message_entry.edit_modified(False)

    def update_location(self):
        dirname = filedialog.askdirectory(parent=self,initialdir='/',title='Please select a directory')
        if dirname != '':
            self.save_location.set(dirname)


    def update_key_options(self):
        if self.algorithm_choice.get() == 'DES3':
            self.key_length_choice.set(KEY_DES3[0])
            menu = self.key_length_choice_option_menu['menu']
            menu.delete(0, 'end')
            for key_length in KEY_DES3:
                menu.add_command(label=key_length, command= lambda key=key_length: self.key_length_choice.set(key))
        if self.algorithm_choice.get() == 'AES':
            self.key_length_choice.set(KEY_AES[0])
            menu = self.key_length_choice_option_menu['menu']
            menu.delete(0, 'end')
            for key_length in KEY_AES:
                menu.add_command(label=key_length, command= lambda key=key_length: self.key_length_choice.set(key))


    
    def encrypt(self):
        self.progress_bar.start_working()

        ### create crypted-message.x

        location = self.save_location.get()

        if location[-1] != '\\':
            file_name = location + '\\' + 'crypted-message.x'
        else:
            file_name = location + 'crypted-message.x'
        
        file = open(file_name, 'w')
        # header
        file.write('---BEGIN OS2 CRYPTO DATA---\n')
        file.write('Description:\n')
        file.write('\tCrypted file\n\n')

        ##!------------------------------------------------------->
        # encryption
        ##!------------------------------------------------------->


        key_length = int(self.key_length_choice.get())
        algorithm = self.algorithm_choice.get()
        mode = self.algorithm_mode.get()

        data = self.message_entry.get('1.0','end-1c')
        data_in_bytes = data.encode('ascii')

        ## DES3 needs 8 byte blocks while AES   need 16 bytes

        block_size = 8

        if(algorithm == 'AES'):
            block_size = 16

        while(len(data_in_bytes) % block_size != 0):
            data_in_bytes += b' '


        if(algorithm == 'DES3'):
            key_length_in_bits = key_length + (KEY_DES3.index(key_length) + 1)*8

            #if desired key_lenght_in_bits is 64 DES3 -> DES
            if(key_length_in_bits == 64):
                key = Random.get_random_bytes(int(key_length_in_bits/8))
                algorithm = 'DES'
            else:
                key = DES3.adjust_key_parity(Random.get_random_bytes(int(key_length_in_bits/8)))
        
        if(algorithm == 'AES'):
            key = Random.get_random_bytes(int(key_length/8))

        key_in_hex = key.hex()    

        #if mode is different from ECB  we also need iv

        iv = None

        if(mode != 'ECB'):
            if algorithm == 'AES':
                iv = Random.get_random_bytes(16)
            else:
                iv = Random.get_random_bytes(8)
            iv_hex = iv.hex()


        cipher_algorithm = ALGORITHMS_DICT[algorithm]
        cipher_mode = MODES_DICT[algorithm][mode]

        if(iv == None):
            cipher = cipher_algorithm.new(key,cipher_mode)
        else:
            cipher = cipher_algorithm.new(key,cipher_mode,iv)

        data_encrypted = cipher.encrypt(data_in_bytes)
        data_b64 = base64.b64encode(data_encrypted)

        ##!--------------------------------------------------->
        # Write stuff
        ##!--------------------------------------------------->

        file.write('Method:\n')
        file.write('\t{}\n\n'.format(algorithm))

        file.write('Mode:\n')
        file.write('\t{}\n\n'.format(mode))

        file.write('File name:\n')
        file.write('\t{}\n\n'.format('crypted-message.x'))


        file.write('Data:\n')

        data_b64 = data_b64.decode()
        
        n = 60
        l = [data_b64[i:i+n] for i in range(0, len(data_b64), n)]
        for line in l:
            file.write('\t{}\n'.format(line))
        file.write('\n')

        if iv != None:
            file.write('Initialization vector:\n')
            file.write('\t{}\n\n'.format(iv_hex))

        file.write('---END OS2 CRYPTO DATA---')
        file.close()

        ### END crypted-message.x

        ### create secret-key.x

        file_name = location + '\\' + 'secret-key.x'
        file = open(file_name, 'w')

        file.write('---BEGIN OS2 CRYPTO DATA---\n')
        file.write('Description:\n')
        file.write('\tSecret key\n\n')

        file.write('Method:\n')
        file.write('\t{}\n\n'.format(algorithm))

        file.write('Mode:\n')
        file.write('\t{}\n\n'.format(mode))

        file.write('Secret key:\n')
        file.write('\t{}\n\n'.format(key_in_hex))

        file.write('---END OS2 CRYPTO DATA---')
        file.close()


