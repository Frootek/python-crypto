
import tkinter as tk
from tkinter import filedialog

from tkinter import ttk

import threading

from progress_bar import AnalyzeProgressBar
from Crypto import Random

from Crypto.PublicKey import RSA
from encryption_methods import generate_rsa_key_pair

import base64

SMALL_FONT = ('Verdana', 5)
MEDIUM_FONT = ('Verdana', 10)
LARGE_FONT  = ('Verdana', 20)
LARGE_LARGE_FONT  = ('Verdana', 30)


KEY_RSA = [1024,2048,3072]



class RSAKeyGenerator(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.label_frame_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_title.pack(fill='x', padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_title,text = 'RSA KEY GENERATOR', font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()

        ##!-------------------------------------------->
        ##KEY LENGTH OPTION
        ##!-------------------------------------------->

        self.label_frame_1 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill='x', padx = 45, pady=5)

        self.key_length_label = tk.Label(self.label_frame_1, text = 'Key length', 
        font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.key_length_label.pack(side = tk.LEFT)

        self.key_length_choice = tk.StringVar(value = KEY_RSA[0])

        self.key_length_choice_option_menu = tk.OptionMenu(self.label_frame_1, self.key_length_choice, *KEY_RSA)
        self.key_length_choice_option_menu.configure(width = 15)
        self.key_length_choice_option_menu.pack(side = tk.LEFT)

        ###SAVE LOCATION

        self.label_frame_2 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill='x', padx = 45, pady=5)

        self.file_save_loc_label = tk.Label(self.label_frame_2, text = 'Save location', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.file_save_loc_label.pack(side = tk.LEFT)

        self.save_location = tk.StringVar(value = 'C:\\')

        self.location_readonly = tk.Entry(self.label_frame_2, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.location_readonly.config(textvariable = self.save_location, relief='groove')
        self.location_readonly.pack(side = tk.LEFT, padx = 2)

        self.change_loc_btn = tk.Button(self.label_frame_2 , text = 'Change', command = lambda : self.update_location())
        self.change_loc_btn.pack(side = tk.LEFT, padx = 10)

        ### GENERTE BTN

        self.label_frame_3 = tk.LabelFrame(self,width=100, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_3.pack(fill='x', padx = 5, pady=5)


        self.encrypt_btn = tk.Button(self.label_frame_3,text = 'GENERATE', 
        command = lambda : self.generate_key_pair())
        self.encrypt_btn.pack(fill='x', padx = 5, pady=5)

        ### Progress bar
        self.label_frame_5 = tk.LabelFrame(self,width=100, height=415,borderwidth=0,highlightthickness=0)
        self.label_frame_5.pack_propagate(0)
        self.label_frame_5.pack(fill='x', padx = 5, pady=5)

        self.progress_bar = AnalyzeProgressBar(self.label_frame_5,self)

        ##back btn
        self.label_frame_6 = tk.LabelFrame(self,width=100, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_6.pack(fill='x', padx = 5, pady=5)

        self.back_btn = tk.Button(self.label_frame_6,text = 'Main page', command = lambda : self.controller.show_frame('MainPage'))
        self.back_btn.pack()


    def generate_key_pair(self):
        self.progress_bar.start_working()
        
        progress_thread = threading.Thread(target = generate_rsa_key_pair,
        args = (self,int(self.key_length_choice.get()),self.save_location.get()))
        progress_thread.start()

    def update_location(self):
        dirname = filedialog.askdirectory(parent=self,initialdir='/',title='Please select a directory')
        if dirname != '':
            self.save_location.set(dirname)

