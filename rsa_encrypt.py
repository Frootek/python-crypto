import tkinter as tk
from tkinter import filedialog
from tkinter import ttk

from progress_bar import AnalyzeProgressBar

from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP

import base64

from parse_file import parse_rsa_file

SMALL_FONT = ('Verdana', 5)
MEDIUM_FONT = ('Verdana', 10)
LARGE_FONT  = ('Verdana', 20)
LARGE_LARGE_FONT  = ('Verdana', 30)



class RSAEncrypt(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.label_frame_title =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_title.pack(fill='x', padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_title,text = 'RSA ENCRYPTION', font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()


        ## form for RSA encryption
        #KEY OPTION ! MOVED TO KEY GEN !

        #-----------------PUBLIC-KEY-lOCATION-------------------->

        self.label_frame_2 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill='x', padx = 45, pady=5)

        self.public_key_loc_label = tk.Label(self.label_frame_2, text = 'Public key', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.public_key_loc_label.pack(side = tk.LEFT)

        self.public_key_location = tk.StringVar()
        self.public_key_location.trace('w', self.verify)

        self.location_readonly = tk.Entry(self.label_frame_2, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.location_readonly.config(textvariable = self.public_key_location, relief='groove')
        self.location_readonly.pack(side = tk.LEFT, padx = 2)

        self.change_loc_btn = tk.Button(self.label_frame_2 , text = 'Import', command = lambda : self.import_public_key())
        self.change_loc_btn.pack(side = tk.LEFT, padx = 10)


        #-----------------SAVE-lOCATION-------------------->
        ###SAVE LOCATION

        self.label_frame_2_5 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_2_5.pack(fill='x', padx = 45, pady=5)

        self.file_save_loc_label = tk.Label(self.label_frame_2_5, text = 'Save location', font = MEDIUM_FONT, width = 15, anchor = 'w')
        self.file_save_loc_label.pack(side = tk.LEFT)

        self.save_location = tk.StringVar(value = 'C:\\')
        self.save_location.trace('w', self.verify)

        self.location_readonly = tk.Entry(self.label_frame_2_5, state = 'readonly',readonlybackground='SystemButtonFace', fg='black')
        self.location_readonly.config(textvariable = self.save_location, relief='groove')
        self.location_readonly.pack(side = tk.LEFT, padx = 2)

        self.change_loc_btn = tk.Button(self.label_frame_2_5 , text = 'Change', command = lambda : self.update_location())
        self.change_loc_btn.pack(side = tk.LEFT, padx = 10)


        #-----------------MESSAGE-BOX-------------------->

        self.label_frame_3 = tk.LabelFrame(self,width=100, height=125,borderwidth=1,
        highlightthickness=0, text = 'Message', font=MEDIUM_FONT,labelanchor = 'n',
        relief = tk.FLAT)
        self.label_frame_3.pack(fill='x', padx = 5, pady=5)
        
        self.message_entry = tk.Text(self.label_frame_3,height = 10,
        fg='black',highlightthickness=0,borderwidth=0)
        self.message_entry.pack(fill='both',expand = 1, padx=10, pady=10)

        self.message_entry.bind('<<Modified>>', self.verify)



        ##-----------------ENCRYPT-BTN-------------------->

        self.label_frame_4 = tk.LabelFrame(self,width=100, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_4.pack(fill='x', padx = 5, pady=5)


        self.encrypt_btn = tk.Button(self.label_frame_4,state='disabled',text = 'Encrypt', command = lambda : self.encrypt())
        self.encrypt_btn.pack(fill='x', padx = 5, pady=5)


        #some space for final button frame
        self.label_frame_5 = tk.LabelFrame(self,width=100, height=210,borderwidth=0,highlightthickness=0)
        self.label_frame_5.pack_propagate(0)
        self.label_frame_5.pack(fill='x', padx = 5, pady=5)

        self.progress_bar = AnalyzeProgressBar(self.label_frame_5,self)


        self.label_frame_6 = tk.LabelFrame(self,width=100, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_6.pack(fill='x', padx = 5, pady=5)

        self.back_btn = tk.Button(self.label_frame_6,text = 'Main page', command = lambda : self.controller.show_frame('MainPage'))
        self.back_btn.pack()



    def encrypt(self):
        self.progress_bar.start_working()
        """
        print("-" * 40)
        print(self.message_entry.get('1.0','end-1c'))
        print("-" * 40)
        print(self.public_key_location.get())
        """
        
        message = self.message_entry.get('1.0','end-1c').encode('ascii')
        public_key_dict = parse_rsa_file(self.public_key_location.get())

        """         
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(public_key_dict)
        """
        ##encrypt data using that public key

        public_key_hex = public_key_dict.get('Public key')

        if(public_key_hex==None):
            print('No public key in file: {}'.format(self.public_key_location.get()))
            return

        public_key = bytes.fromhex(public_key_hex).decode('ascii')
        self.data = b""

        recipient_key = RSA.import_key(public_key)
        session_key = get_random_bytes(16)

        # Encrypt the session key with the public RSA key
        cipher_rsa = PKCS1_OAEP.new(recipient_key)
        enc_session_key = cipher_rsa.encrypt(session_key)

        # Encrypt the data with the AES session key
        cipher_aes = AES.new(session_key, AES.MODE_EAX)
        ciphertext, tag = cipher_aes.encrypt_and_digest(message)

        [ self.string_concat(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext) ]

        self.data = base64.b64encode(self.data)

        
        ###write data to file

        location = self.save_location.get()

        if location[-1] != '\\':
            file_name = location + '\\' + 'rsa-crypted-message.x'
        else:
            file_name = location + 'rsa-crypted-message.x'

        file = open(file_name, 'w')

        file.write('---BEGIN OS2 CRYPTO DATA---\n')
        file.write('Description:\n')
        file.write('\tCrypted file\n\n')

        file.write('Method:\n')
        file.write('\tRSA\n\n')

        file.write('Data:\n')

        self.write_format_max_char_length(file,self.data)

        file.write('---END OS2 CRYPTO DATA---')
        file.close()
    


    def import_public_key(self):
        filename = filedialog.askopenfile(initialdir = '/', title = 'Select file')
        if filename.name != '':
            self.public_key_location.set(filename.name)

    def verify(self,*args):
        text = self.message_entry.get('1.0','end-1c')
        file = self.public_key_location.get()
        save_loc = self.save_location.get()
        if (file[-2:] == '.x' or file[-4:] == '.txt') and (len(text) > 0) and (len(save_loc)>0):
            self.encrypt_btn.config(state='normal')
        else:
            self.encrypt_btn.config(state='disabled')
        self.message_entry.edit_modified(False)

    def update_location(self):
        dirname = filedialog.askdirectory(parent=self,initialdir='/',title='Please select a directory')
        if dirname != '':
            self.save_location.set(dirname)
    
    def string_concat(self, add):
        self.data = self.data + add

    def write_format_max_char_length(self, file, data, n=60):
        l = [data[i:i+n] for i in range(0, len(data), n)]
        for line in l:
            file.write('\t{}\n'.format(line.decode()))
        file.write('\n')